const express = require('express');
const router = express.Router();
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');

// ****************** SETTING GLOBAL ENV ******************
global.ENV = process.env.NODE_ENV || 'development';
console.log('APP JS ENVIRONMENT: ', ENV);

const mongoConfig = require('./config/components/mongo');
var index = require('./routes/index');
var userWellness = require('./routes/userWellness');
var userSrmd = require('./routes/userSrmd');
var userOnetake = require('./routes/userOnetake');
var userHarekrishna = require('./routes/userHarekrishna');
var harekrishnadata = require('./routes/harekrishnadata');


/**
 * @val - Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);
  if (typeof (port) !== 'number') {
    // named pipe
    return val;
  }
  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}

const port = normalizePort(process.env.PORT || '3007');
console.log(`NODE LISTENING ON PORT ${port}`);

const app = express();
mongoConfig.bootstrap();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(logger('dev'));

app.use(bodyParser.json({ limit: '50mb' }));
// app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
//   limit: '26000mb',
//   extended: true,
// }));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));



app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: 'live-trim-session-parameter',
  resave: true,
  saveUninitialized: true
}));

app.use('/v1', router);
app.use('/', index);


app.use((req, res, next) => {
  console.log(req.session.user_id);
  if (req.session.user_id == null) {
    // if user is not logged-in redirect back to login page //
    res.redirect('/');
  } else {
    next();
  }
});

app.use('/v1/userWellness', userWellness);
app.use('/v1/userSrmd', userSrmd);
app.use('/v1/userOnetake', userOnetake);
app.use('/v1/userHarekrishna', userHarekrishna);
app.use('/v1/harekrishnadata', harekrishnadata);



// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  console.log(`ERRR APPP USE!! ${err}`);
  next(err);
});


// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port);

mongoose.set('debug', true) // for dev only

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  // eslint-disable-next-line no-undef
  const addr = server.address();
  const bind = typeof addr === 'string' ? (`pipe ${addr}`) : `port ${addr.port}`;

  // eslint-disable-next-line no-undef
  console.log(`Listening on ${bind}`);
}
app.on('listening', onListening);

module.exports = app;
