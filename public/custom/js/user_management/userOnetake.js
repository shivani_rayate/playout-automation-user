var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/userOnetake/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data
            } else {
                var GetOnetakefileData = data.data
                appendFiledatabyfileNamewithOnetake(GetOnetakefileData)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


var filename = '';
function appendFiledatabyfileNamewithOnetake(GetOnetakefileData) {
    if (GetOnetakefileData !== null) {

        var array = GetOnetakefileData.fileData;
        filename = GetOnetakefileData.fileName;
        var options_table = "";

        array.forEach(function (element, i) {

            var StartDate = element.StartDate ? element.StartDate : "-";
            var ContentID = element.ContentID ? element.ContentID : "-";
            var ProgramName = element.ProgramName ? element.ProgramName : "-";
            var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
            var OnAirTime = element.OnAirTime ? element.OnAirTime : "-";
            var DURATION = element.Duration ? element.Duration : "-";
            var SrNo = element.SrNo ? element.SrNo : "-";


            options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>

                       <td class="SrNo" data-SrNo="${SrNo}">${SrNo}</td>
                       <td class="Channel" id="StartDate">${StartDate}</td>
                       <td class="name" id="ContentID">${ContentID}</td>
                       <td class="name" id="ProgramName">${ProgramName}</td>
                       <td class="name" id="ProgramFile">${ProgramFile}</td>
                       <td class="name" id="OnAirTime">${OnAirTime}</td>
                       <td class="name" id="DURATION">${DURATION}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#onetakexldata_tbody').append(options_table);

                $("#onetakexldata_table").Tabledit({
                    buttons: {
                        edit: {
                            class: "btn btn-sm btn-info edit-xls-btn",
                            html: `<span class="mdi mdi-pencil"></span>`,
                            action: "edit",
                        },
                        save: {
                            class: 'btn btn-sm btn-success Onetake-save-xls-btn',
                            html: `<span class="mdi mdi-check"></span>`
                        },
                    },
                    inputClass: "form-control form-control-sm",
                    deleteButton: !1,
                    autoFocus: !1,
                    columns: {
                        identifier: [0, "id"],
                        editable: [
                            [1, "col1"],
                            [2, "col2"],
                            [3, "col3"],
                            [4, "col4"],
                            [5, "col5"],
                            [6, "col6"]
                        ]
                    }
                })
            }
        })
    }
}


$(document).ready(function () {

    $("#uploadFormOnetake").submit(function (event) {

        var formData = new FormData();
        formData.append('userOnetake', $('#userOnetake')[0].files[0]);
        formData.append('channel_name', $('option:selected', this).text() ? $('option:selected', this).text() : null);
        $.ajax({
            url: '/v1/userOnetake/fileOnetake',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
            if (data.status === 200 || data) {

                var fileUpload = document.getElementById("userOnetake");

                //Validate whether File is valid Excel file.
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();

                    //For Browsers other than IE.
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e) {
                            ProcessExcel(e.target.result);
                        };
                        reader.readAsBinaryString(fileUpload.files[0]);
                    } else {
                        //For IE Browser.
                        reader.onload = function (e) {
                            var data = "";
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            ProcessExcel(data);
                        };
                        reader.readAsArrayBuffer(fileUpload.files[0]);
                    }
                } else {
                    alert("This browser does not support HTML5.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })


        var formData1 = new FormData();
        formData1.append('userOnetakeEpg', $('#userOnetakeEpg')[0].files[0]);
        $.ajax({
            url: '/v1/userOnetake/fileOnetakeEpg',
            type: 'POST',
            data: formData1,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
            if (data.status === 200 || data) {
                var fileUpload1 = document.getElementById("userOnetakeEpg");

                //Validate whether File is valid Excel file.
                if (fileUpload1.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel1(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload1.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel1(data);
                            };
                            reader.readAsArrayBuffer(fileUpload1.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    function ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        var ex = Object.keys(excelRows[0])

        if (ex[0] === "SrNo" && ex[1] === "Name" && ex[2] === "StartDate" && ex[3] === "ContentID" && ex[4] === "ProgramName" && ex[5] === "Next" && ex[6] === "ProgramFile" && ex[7] === "InPoint" && ex[8] === "OutPoint" && ex[9] === "OnAirTime" && ex[10] === "Duration") {

            var params = {
                fileData: excelRows
            }


            $.post('/v1/userOnetake/saveplaylist', params, function (data, status) {
                if (data.status == 200) {
                    toastr.success('File Uploaded Successfully');
                    setTimeout(function () {
                        window.location.href = '/v1/userOnetake/userOnetake'
                    }, 2000);
                }
            })
        } else {
            toastr.error("Playout Excel Column name head spelling are not correct or not as per documentation")
        }
    }

    function ProcessExcel1(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        var params = {
            fileData1: excelRows
        }
        $.post('/v1/userOnetake/saveEpg', params, function (data, status) {
            if (data.status == 200) {

            }
        })
    }


    $.get('/v1/userOnetake/datatodisplay', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });

    function destroyRowsxllist() {
        $('#xllist_tbody').empty()
        $('#xllist_table').DataTable().rows().remove();
        $("#xllist_table").DataTable().destroy()
    }

    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {

                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
              <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="/v1/userOnetake/userOnetake-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            <a href="/v1/userOnetake/epg_Onetake?fileName=${fileName}" key-value="${fileName}" class="dropdown-item">View Epg</a> </div> </div></div> </div></td>`;

                if (i == array.length - 1) {
                    $('#xllist_tbody').append(uploaded_list);
                    reInitialiClientSheetListTable()
                }
            })
        }
    }


    function reInitialiClientSheetListTable() {
        $("#xllist_table").DataTable().destroy()
        xllist_table = $('#xllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#xllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {

                $.post('/v1/userOnetake/deletefile', params, function (data, status) {
                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })
})


$(document).on("click", ".Onetake-save-xls-btn", function (event) {

    const params = {

        SrNo: $(this).closest("tr").find("td.SrNo").attr('data-SrNo'),
        StartDate: $(this).closest("tr").find("#StartDate").text(),
        ContentID: $(this).closest("tr").find("#ContentID").text(),
        ProgramName: $(this).closest("tr").find("#ProgramName").text(),
        ProgramFile: $(this).closest("tr").find("#ProgramFile").text(),
        OnAirTime: $(this).closest("tr").find("#OnAirTime").text(),
        DURATION: $(this).closest("tr").find("#DURATION").text(),
        fileName: filename,
    }
    $.post('/v1/userOnetake/onetakeDataUpdate',
        params,
        function (data, status) {
            if (data.status === 200) {
               
            }
        })
})

// Get EPG data
$.get('/v1/userOnetake/getEPGxdata', function (data, status) {
    if (data.status == 200) {
        destroyRowsEPGlist()
        appendEPGData(data)
    }
});

function destroyRowsEPGlist() {
    $('#onetake_epglist_tbody').empty()
    $('#onetake_epg_list_table').DataTable().rows().remove();
    $("#onetake_epg_list_table").DataTable().destroy()
}

function appendEPGData(data) {
    var array = data.data;
    if (array.length) {
        var uploaded_list = "";
        array.forEach(function (element, i) {
            var fileName = element.fileName ? element.fileName : "";
            var created_at = element.created_at ? moment(element.created_at).format('lll') : "";
            var playlistFilename = element.playlistFilename ? element.playlistFilename : "";

            uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
            <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="/v1/userOnetake/epg_Onetake?fileName=${playlistFilename}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete_Epg">Delete</a> 
            <a  href="/v1/userOnetake/userOnetake-xlsheetdata?fileName=${playlistFilename}" key-value="${fileName}" class="dropdown-item ">View Playlist</a> </div> </div>
            </td>`
            if (i == array.length - 1) {
                $('#onetake_epglist_tbody').append(uploaded_list);
                reInitializeEPGDataTable()
            }
        })
    }
}


function reInitializeEPGDataTable() {
    $("#onetake_epg_list_table").DataTable().destroy()
    xllist_table = $('#onetake_epg_list_table').DataTable({
        //"order": [[1, "desc"]], // for descending order
        "columnDefs": [
            { "width": "30%", "targets": 1 }
        ]
    })
    $("#onetake_epg_list_table tbody tr:first").addClass("active");
}

// Delete file
$(document).on("click", ".delete_Epg", function (event) {

    var File_name = $(this).attr('key-value');
    let params = {
        File_name: File_name
    }

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {

            $.post('/v1/userOnetake/deleteEpg', params, function (data, status) {

                if (data.status == 200) {
                    toastr.success('File Deleted');

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000)
                    event.preventDefault()
                }
            });
        }
    })
})