
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;


$.get(`/v1/userWellness/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data;
            } else {
                var GetfileData = data.data;
                appendFiledatabyfileName(GetfileData)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


var filename = '';
function appendFiledatabyfileName(GetfileData) {
    if (GetfileData !== null) {

        var array = GetfileData.fileData;
        filename = GetfileData.fileName;
        var options_table = "";

        array.forEach(function (element, i) {

            var SrNo = element.SrNo ? element.SrNo : "-";
            var ProgrammeName = element.ProgrammeName ? element.ProgrammeName : "-";
            var TOPIC = element.TOPIC ? element.TOPIC : "-";
            var Subject = element.Subject ? element.Subject : "-";
            var CodeNo = element.CodeNo ? element.CodeNo : "-";
            var Duration = element.Duration ? element.Duration : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                   <td class="${(i + 1)} SrNo" data-eventId="${SrNo}">${SrNo}</td>
                   <td class="name" id="ProgrammeName">${ProgrammeName}</td>
                   <td class="name" id="TOPIC">${TOPIC}</td>
                   <td class="name" id="Subject">${Subject}</td>
                   <td class="name" id="CodeNo">${CodeNo}</td>
                   <td class="name" id="Duration">${Duration}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#xldata_tbody').append(options_table);

                $("#xldata_table").Tabledit({
                    buttons: {
                        edit: {
                            class: "btn btn-sm btn-info edit-xls-btn",
                            html: `<span class="mdi mdi-pencil"></span>`,
                            action: "edit",
                        },
                        save: {
                            class: 'btn btn-sm btn-success Wellness-save-xls-btn',
                            html: `<span class="mdi mdi-check"></span>`
                        },
                    },
                    inputClass: "form-control form-control-sm",
                    deleteButton: !1,
                    autoFocus: !1,
                    columns: {
                        identifier: [0, "id"],
                        editable: [
                            [1, "col1"],
                            [2, "col2"],
                            [3, "col3"],
                            [4, "col4"],
                            [5, "col5"]
                        ]
                    }
                })

            }
        })
    }
}


$(document).ready(function () {

    $("#uploadForm").submit(function (event) {
        var formData = new FormData();
        formData.append('userPhoto', $('#userPhoto')[0].files[0]);
        $.ajax({
            url: '/v1/userWellness/fileWellness',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {

            if (data.status === 200 || data) {

                var fileUpload = document.getElementById("userPhoto");
                //Validate whether File is valid Excel file.
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();

                    //For Browsers other than IE.
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e) {
                            ProcessExcel(e.target.result);
                        };
                        reader.readAsBinaryString(fileUpload.files[0]);
                    } else {
                        //For IE Browser.
                        reader.onload = function (e) {
                            var data = "";
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            ProcessExcel(data);
                        };
                        reader.readAsArrayBuffer(fileUpload.files[0]);
                    }
                } else {
                    alert("This browser does not support HTML5.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })


        var formData1 = new FormData();
        formData1.append('videoUpload', $('#input1')[0].files[0]);
        $.ajax({
            url: '/v1/userWellness/fileWellnessepg',
            type: 'POST',
            data: formData1,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
            if (data.status === 200 || data) {

                var fileUpload1 = document.getElementById("input1");

                //Validate whether File is valid Excel file.
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();

                    //For Browsers other than IE.
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e) {
                            ProcessExcel1(e.target.result);
                        };
                        reader.readAsBinaryString(fileUpload1.files[0]);
                    } else {
                        //For IE Browser.
                        reader.onload = function (e) {
                            var data = "";
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            ProcessExcel1(data);
                        };
                        reader.readAsArrayBuffer(fileUpload1.files[0]);
                    }
                } else {
                    alert("This browser does not support HTML5.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    function ProcessExcel(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        var ex = Object.keys(excelRows[0])

        if (ex[0] === "SrNo" && ex[1] === "ProgrammeName" && ex[2] === "TOPIC" && ex[3] === "CodeNo" && ex[4] === "Duration" || ex[0] === "SrNo" && ex[1] === "ProgrammeName" && ex[2] === "TOPIC" && ex[4] === "CodeNo" && ex[5] === "Duration" && ex[3] === "Subject") {

            var params = {
                fileData: excelRows
            }
            $.post('/v1/userWellness/saveplaylist', params, function (data, status) {
                if (data.status == 200) {
                    toastr.success('File Uploaded Successfully');
                    setTimeout(function () {
                        window.location.href = '/v1/userWellness/userWellness'
                    }, 2000);
                }
            })
        } else {
            toastr.error("Playout Excel Column name head spelling are not correct or not as per documentation")
        }
    }


    function ProcessExcel1(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });
        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        var params = {
            fileData1: excelRows
        }
        $.post('/v1/userWellness/saveEpg', params, function (data, status) {
            if (data.status == 200) {
            }
        })
    }


    // get List of Wellness
    $.get('/v1/userWellness/getXslxdata', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });
    function destroyRowsxllist() {
        $('#xllist_tbody').empty()
        $('#xllist_table').DataTable().rows().remove();
        $("#xllist_table").DataTable().destroy()
    }

    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {

                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
            <td class="username " key_factor="${fileName}"><a class="group-name-link"  href="/v1/userWellness/user-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            <a href="/v1/userWellness/epg_data?fileName=${fileName}" key-value="${fileName}" class="dropdown-item">View Epg</a> </div> </div>
            </td>`
                if (i == array.length - 1) {
                    $('#xllist_tbody').append(uploaded_list)
                    reInitialiClientSheetListTable()
                }
            })
        }
    }


    function reInitialiClientSheetListTable() {
        $("#xllist_table").DataTable().destroy()
        xllist_table = $('#xllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#xllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {

                $.post('/v1/userWellness/deletefile', params, function (data, status) {

                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })
})



$(document).on("click", ".Wellness-save-xls-btn", function (event) {

    const params = {
        SrNo: $(this).closest("tr").find("td.SrNo").attr('data-eventId'),
        ProgrammeName: $(this).closest("tr").find("#ProgrammeName").text(),
        TOPIC: $(this).closest("tr").find("#TOPIC").text(),
        Subject: $(this).closest("tr").find("#Subject").text(),
        CodeNo: $(this).closest("tr").find("#CodeNo").text(),
        Duration: $(this).closest("tr").find("#Duration").text(),
        fileName: filename,
    }
    $.post('/v1/userWellness/wellnessDataUpdate',
        params,
        function (data, status) {
            if (data.status === 200) {
               
            }
        })
})

// Get EPG data
$.get('/v1/userWellness/getEPGxdata', function (data, status) {
    if (data.status == 200) {
        destroyRowsEPGlist()
        appendEPGData(data)
    }
});

function destroyRowsEPGlist() {
    $('#wellness_epglist_tbody').empty()
    $('#wellness_epg_list_table').DataTable().rows().remove();
    $("#wellness_epg_list_table").DataTable().destroy()
}

function appendEPGData(data) {
    var array = data.data;
    if (array.length) {
        var uploaded_list = "";
        array.forEach(function (element, i) {
            var fileName = element.fileName ? element.fileName : "";
            var created_at = element.created_at ? moment(element.created_at).format('lll') : "";
            var playlistFilename = element.playlistFilename ? element.playlistFilename : "";

            uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
        <td class="">${(i + 1)}</td>
        <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="/v1/userWellness/epg_data?fileName=${playlistFilename}">${fileName}</a></td>
        <td class="name">${created_at}</td>
        <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
        <a href="#" key-value="${fileName}" class="dropdown-item delete_Epg">Delete</a> 
        <a  href="/v1/userWellness/user-xlsheetdata?fileName=${playlistFilename}" key-value="${fileName}" class="dropdown-item ">View Playlist</a> </div> </div>
        </td>`
            if (i == array.length - 1) {
                $('#wellness_epglist_tbody').append(uploaded_list);
                reInitializeEPGDataTable()
            }
        })
    }
}


function reInitializeEPGDataTable() {
    $("#wellness_epg_list_table").DataTable().destroy()
    xllist_table = $('#wellness_epg_list_table').DataTable({
        //"order": [[1, "desc"]], // for descending order
        "columnDefs": [
            { "width": "30%", "targets": 1 }
        ]
    })
    $("#wellness_epg_list_table tbody tr:first").addClass("active");
}


// Delete file
$(document).on("click", ".delete_Epg", function (event) {

    var File_name = $(this).attr('key-value');
    let params = {
        File_name: File_name
    }

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {

            $.post('/v1/userWellness/deleteEpg', params, function (data, status) {

                if (data.status == 200) {
                    toastr.success('File Deleted');

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000)
                    event.preventDefault()
                }
            });
        }
    })
})
