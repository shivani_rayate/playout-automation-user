

var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/userSrmd/getFileDatabyEpgFilename?fileName=${fileName}`,
function (data, status) {
    if (data.status === 200) {

        if (data === null) {
            var data1 = data.data
        } else {
            var GetSrmdfileDataEpg = data.data;
            appendFiledataSrmdEpgfileName(GetSrmdfileDataEpg)
        }
    } else {
        console.log("get getFileDatabyFilename Failed")
    }
})


function appendFiledataSrmdEpgfileName(GetSrmdfileDataEpg) {
if (GetSrmdfileDataEpg !== null) {

    var array = GetSrmdfileDataEpg.fileData;
    var options_table = "";

    array.forEach(function (element, i) {

        var Date = element.Date ? element.Date : "-";
        var Start_Time = element.StartTime ? element.StartTime : "-";
        var Stop_Time = element.StopTime ? element.StopTime : "-";
        var DuraTion = element.Duration ? element.Duration : "-";
        var Title = element.Title ? element.Title : "-";
        var DescriPtion = element.Description ? element.Description : "-";
        var Is_repeat = element.Isrepeat ? element.Isrepeat : "-";
        var Thumbnail = element.Thumbnail ? element.Thumbnail : "-";
        var Program_Number = element.ProgramNumber ? element.ProgramNumber : "-";

        options_table += `<tr class="users-tbl-row asset-row">
                   <td class="index">${i + 1}</td>
                   <td class="date">${Date}</td>
                   <td class="name">${Start_Time}</td>
                   <td class="name">${Stop_Time}</td>
                   <td class="name">${DuraTion}</td>
                   <td class="name">${Title}</td>
                   <td class="name" >${DescriPtion}</td>
                   <td class="name">${Is_repeat}</td>
                   <td class="text-break">${Thumbnail}</td>
                   <td class="name">${Program_Number}</td>`;

        if (i === array.length - 1) {
            //initiate for 1st row
            $('#epgSrmddata_tbody').append(options_table);
        }
    })
}
}