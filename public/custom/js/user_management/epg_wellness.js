
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/userWellness/getFileDatabyEpgFilename?fileName=${fileName}`,
function (data, status) {
    if (data.status === 200) {
        if (data === null) {
            var _data = data.data
        } else {
            var GetfileDataEpg = data.data
            appendFiledatabyEpgfileName(GetfileDataEpg)
        }
    } else {
        console.log("get getFileDatabyFilename Failed")
    }
})


function appendFiledatabyEpgfileName(GetfileDataEpg) {
if (GetfileDataEpg !== null) {

    var array = GetfileDataEpg.fileData
    var options_table = "";

    array.forEach(function (element, i) {

        var Date = element.Date ? element.Date : "-";
        var Start_Time = element.StartTime ? element.StartTime : "-";
        var Stop_Time = element.StopTime ? element.StopTime: "-";
        var Duration = element.Duration ? element.Duration : "-";
        var Title = element.Title ? element.Title : "-";
        var Description = element.Description ? element.Description : "-";
        var Isrepeat = element.Isrepeat ? element.Isrepeat : "-";
        var Thumbnail = element.Thumbnail ? element.Thumbnail : "-";
        var Program_Number = element.ProgramNumber ? element.ProgramNumber : "-";

        options_table += `<tr class="users-tbl-row asset-row">
               <td class="index">${(i + 1)}</td>
               <td class="name">${Date}</td>
               <td class="name">${Start_Time}</td>
               <td class="name">${Stop_Time}</td>
               <td class="name">${Duration}</td>
               <td class="name">${Title}</td>
               <td class="name">${Description}</td>
               <td class="name">${Isrepeat}</td>
               <td class="name">${Thumbnail}</td>
               <td class="name">${Program_Number}</td>`;

        if (i == array.length - 1) {
            //initiate for 1st row
            $('#epgdata_tbody').append(options_table);

        }
    })
}
}
