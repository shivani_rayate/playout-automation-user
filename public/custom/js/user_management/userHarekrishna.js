
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/userHarekrishna/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data;
            } else {
                var GetHarefileData = data.data;
                appendFiledatabyfileNamewithHare(GetHarefileData)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })

var filename = '';
function appendFiledatabyfileNamewithHare(GetHarefileData) {
    if (GetHarefileData !== null) {

        var array = GetHarefileData.fileData;
        filename = GetHarefileData.fileName;
        var options_table = "";

        array.forEach(function (element, i) {

            var SrNo = element["SrNo"] ? element["SrNo"] : "-";
            var start_time = element["AiringStartTime"] ? element["AiringStartTime"] : "-";
            var end_time = element["EndTime"] ? element["EndTime"] : "-";
            var ProgramName = element["ProgramName"] ? element["ProgramName"] : "-";
            var FileName = element["FileName"] ? element["FileName"] : "-";
            var FillersSerialNumber = element["FillersSerialNumber"] ? element["FillersSerialNumber"] : "-";
            var POPUPImage_Video_1 = element["POPUPImage/Video_1"] ? element["POPUPImage/Video_1"] : "-";
            var POPUPImage_Video_2 = element["POPUPImage/Video_2"] ? element["POPUPImage/Video_2"] : "-";
            var POPUPImage_Video_3 = element["POPUPImage/Video_3"] ? element["POPUPImage/Video_3"] : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                           <td class="${(i + 1)} SrNo" data-eventId="${SrNo}">${SrNo}</td>
                           <td class="start_time" id="start_time">${start_time}</td>
                           <td class="end_time" id="end_time">${end_time}</td>
                           <td class="name" id="ProgramName">${ProgramName}</td>
                           <td class="name" id="FileName">${FileName}</td>
                           <td class="name" id="FillersSerialNumber">${FillersSerialNumber}</td>
                           <td class="name" id="POPUPImage_Video_1">${POPUPImage_Video_1}</td>
                           <td class="name" id="POPUPImage_Video_2">${POPUPImage_Video_2}</td>
                           <td class="name" id="POPUPImage_Video_3">${POPUPImage_Video_3}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#harexldata_tbody').append(options_table);

                $("#harexldata_table").Tabledit({
                    buttons: {
                        edit: {
                            class: "btn btn-sm btn-info edit-xls-btn",
                            html: `<span class="mdi mdi-pencil"></span>`,
                            action: "edit",
                        },
                        save: {
                            class: 'btn btn-sm btn-success hareK-save-xls-btn',
                            html: `<span class="mdi mdi-check"></span>`
                        },
                    },
                    inputClass: "form-control form-control-sm",
                    deleteButton: !1,
                    autoFocus: !1,
                    columns: {
                        identifier: [0, "id"],
                        editable: [
                            [1, "col1"],
                            [2, "col2"],
                            [3, "col3"],
                            [4, "col4"],
                            [5, "col5"],
                            [6, "col6"],
                            [7, "col7"],
                            [8, "col8"]
                        ]
                    }
                })
            }
        })
    }

}


$(document).ready(function () {

    $("#uploadFormHare").submit(function (event) {
        var formData = new FormData();
        formData.append('userHarekrishna', $('#userHarekrishna')[0].files[0]);
        formData.append('channel_name', $('option:selected', this).text() ? $('option:selected', this).text() : null);
        $.ajax({
            url: '/v1/userHarekrishna/fileHare',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {

            if (data.status === 200 || data) {

                var fileUpload1 = document.getElementById("userHarekrishna");
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();

                    //For Browsers other than IE.
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e) {
                            ProcessExcel(e.target.result);
                        };
                        reader.readAsBinaryString(fileUpload1.files[0]);
                    } else {
                        //For IE Browser.
                        reader.onload = function (e) {
                            var data = "";
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            ProcessExcel(data);
                        };
                        reader.readAsArrayBuffer(fileUpload1.files[0]);
                    }
                } else {
                    alert("This browser does not support HTML5.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    function ProcessExcel(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        var params = {
            fileData: excelRows
        }
        $.post('/v1/userHarekrishna/saveplaylist', params, function (data, status) {
            if (data.status == 200) {
                toastr.success('File Uploaded Successfully');
                setTimeout(function () {
                    window.location.href = '/v1/userHarekrishna/userHarekrishna'
                }, 2000);
            }
        })
    }

    // getXslxdata
    $.get('/v1/userHarekrishna/getXslxdata', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });

    function destroyRowsxllist() {
        $('#harexllist_tbody').empty()
        $('#harexllist_table').DataTable().rows().remove();
        $("#harexllist_table").DataTable().destroy()
    }

    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {

                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
            <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="/v1/userHarekrishna/userHarekrishan-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            </td>`
                if (i == array.length - 1) {
                    $('#harexllist_tbody').append(uploaded_list);
                    reInitialiClientSheetListTable()
                }
            })
        }
    }


    function reInitialiClientSheetListTable() {
        $("#harexllist_table").DataTable().destroy()
        xllist_table = $('#harexllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#harexllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {
                $.post('/v1/userHarekrishna/deletefile', params, function (data, status) {
                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })
})



$(document).on("click", ".hareK-save-xls-btn", function (event) {

    const params = {
        SrNo: $(this).closest("tr").find("td.SrNo").attr('data-eventId'),
        start_time: $(this).closest("tr").find("#start_time").text(),
        end_time: $(this).closest("tr").find("#end_time").text(),
        ProgramName: $(this).closest("tr").find("#ProgramName").text(),
        FileName: $(this).closest("tr").find("#FileName").text(),
        FillersSerialNumber: $(this).closest("tr").find("#FillersSerialNumber").text(),
        POPUPImage_Video_1: $(this).closest("tr").find("#POPUPImage_Video_1").text(),
        POPUPImage_Video_2: $(this).closest("tr").find("#POPUPImage_Video_2").text(),
        POPUPImage_Video_3: $(this).closest("tr").find("#POPUPImage_Video_3").text(),
        fileName: filename,
    }

    $.post('/v1/userHarekrishna/hareKDataUpdate',
        params,
        function (data, status) {
            if (data.status === 200) {
              
            }
        })
})