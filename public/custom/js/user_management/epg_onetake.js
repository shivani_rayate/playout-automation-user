
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;


$.get(`/v1/userOnetake/getFileDatabyEpgFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {

            if (data === null) {
                var data1 = data.data
            } else {
                var GetOnetakefileDataEpg = data.data
                appendFiledataOnetakeEpgfileName(GetOnetakefileDataEpg)
            }

        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


function appendFiledataOnetakeEpgfileName(GetOnetakefileDataEpg) {
    if (GetOnetakefileDataEpg !== null) {
        var array = GetOnetakefileDataEpg.fileData;

        var options_table = "";

        array.forEach(function (element, i) {

            var Date = element.Date ? element.Date : "-";
            var Time = element.Time ? element.Time : "-";
            var Duration = element.Duration ? element.Duration : "-";
            var Title = element.Title ? element.Title : "-";
            var Synopsis = element.Synopsis ? element.Synopsis : "-";
            var Genure = element.Genre ? element.Genre : "-";
            var Sub_Genure = element.SubGenre ? element.SubGenre : "-";
            var Language = element.Language ? element.Language : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                   <td class="date">${i + 1}</td>
                   <td class="date">${Date}</td>
                   <td class="name">${Time}</td>
                   <td class="name">${Duration}</td>
                   <td class="name">${Title}</td>
                   <td class="name" >${Synopsis}</td>
                   <td class="name">${Genure}</td>
                   <td class="text-break">${Sub_Genure}</td>
                   <td class="name">${Language}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#epgOnetakedata_tbody').append(options_table);
            }
        })
    }
}
