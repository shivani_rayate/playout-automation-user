var express = require('express');
var router = express.Router();

// ##############################################################################

var fs = require('fs');
const multerS3 = require('multer-s3');
const awsConfig = require('../config/aws_config')['development'];
var multer = require('multer')
var AWS = require('aws-sdk');
var s3 = new AWS.S3();


const cognito = require('../app/utilities/user_management/cognito');

const fileData = require("../workers/fileData");
const fileDataObj = new fileData.fileDataClass();

const fileDataEpg = require("../workers/fileDataEpg");
const fileDataEpgObj = new fileDataEpg.fileDataEpgClass();

const airfileData = require("../workers/airFileData");
const airfileDataObj = new airfileData.airfileDataClass();

/* GET users listing. */
router.get('/', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userWellness/index', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
});


router.get('/user-groups', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userWellness/user-groups', { section: 'Users Listing', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})

router.get('/userWellness', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userWellness/userWellness', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})


router.get('/user-xlsheetdata', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userWellness/user-groups', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})


router.get('/epg_data', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userWellness/epg_data', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})

var EpgfileNameOld = '';
var fileNameInEpg = '';
router.post('/fileWellness', (req, res, next) => {
  let fileNAME = '';
  var date = new Date();
  var time = date.toTimeString();
  var withTime = time.substring(0, 8);
  const upload = multer({
    storage: multerS3({
      s3: s3,
      acl: 'public-read',
      bucket: `${awsConfig.bucket}/${awsConfig.putfolder}`,
      key: (req, file, cb) => {
        console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
        var pieces = `${file.originalname}`.split(".")
        var before = pieces[0]
        var after = pieces[1]
        var adding = before.concat(`_${withTime}`);
        var finalAdd = adding.concat(`.${after}`)
        fileNAME = `${finalAdd}`;
        console.log(`FILE NAME>> ${fileNAME}`);
        cb(null, fileNAME);
      },
    }),
  }).array('userPhoto', 1);

  upload(req, res, (error) => {
    if (error) {
      console.log(`VIDEO UPLOAD ERROR: ${error}`);
    } else {
      console.log('VIDEO UPLOAD DONE!!!!!!');
      res.send("upload")

      fileNameInEpg = fileNAME;

    }
  });
})


router.post('/fileWellnessepg', (req, res, next) => {
  let fileNAME1 = '';
  var date = new Date();
  var time = date.toTimeString();
  var withTime = time.substring(0, 8);
  const upload1 = multer({
    storage: multerS3({
      s3: s3,
      acl: 'public-read',
      bucket: `${awsConfig.bucket}/${awsConfig.putfolder1}`,
      key: (req, file, cb) => {
        console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
        var pieces = `${file.originalname}`.split(".")
        var before = pieces[0]
        var after = pieces[1]
        var adding = before.concat(`_${withTime}`);
        var finalAdd = adding.concat(`.${after}`)
        fileNAME1 = `${finalAdd}`;
        console.log(`FILE NAME>> ${fileNAME1}`);
        cb(null, fileNAME1);
      },
    }),
  }).array('videoUpload', 1);

  upload1(req, res, (error) => {
    if (error) {
      console.log(`VIDEO UPLOAD ERROR: ${error}`);
    } else {
      console.log('VIDEO UPLOAD DONE!!!!!!');
      res.send("upload")
      EpgfileNameOld = fileNAME1;
    }
  });
})

// Save Playlist In MongoDB
router.post('/saveplaylist', (req, res, next) => {

  var fileData = req.body.fileData;
  var fileName = fileNameInEpg;
  var UserName = req.session.clientName;

  const params = {
    fileData: fileData,
    fileName: fileName,
    UserName: UserName,
    channelName: "wellness"
  }
  fileDataObj.save(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'Filedata Saved successfuly',
        data: _response,
      })
    } else {
      console.log('Filedata data save failed');
      res.json({
        status: 401,
        message: 'Filedata Save failed',
        data: null,
      })
    }
  })
})

// Save EPG File In MongoDB
router.post('/saveEpg', (req, res, next) => {

  var fileDataEpg = req.body.fileData1;
  var fileName = fileNameInEpg;
  var UserName = req.session.clientName;
  var EpgfileName = EpgfileNameOld;

  const params = {
    fileDataEpg: fileDataEpg,
    playlistFilename: fileName,
    UserName: UserName,
    EpgfileName: EpgfileName,
    channelName: "wellness"
  }
  fileDataEpgObj.save(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'Filedata Saved successfuly',
        data: _response,
      })
    } else {
      console.log('Filedata data save failed');
      res.json({
        status: 401,
        message: 'Filedata Save failed',
        data: null,
      })
    }
  })
})


// Get All Users
router.get('/getXslxdata', (req, res, next) => {

  const params = {
    UserName: req.session.clientName
  }
  fileDataObj.findFileDataByUserName(params).then((data) => {
    res.json({
      status: 200,
      message: 'Objects fetched successfully',
      data: data,
    });
  });
});


// registration
router.post('/register', (req, res, next) => {

  const params = {
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  };

  cognito.RegisterUser(params).then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
});


// Delete File from s3 then MongoDb
router.post('/deletefile', (req, res, next) => {

  const params = {
    File_name: req.body.File_name ? req.body.File_name : null
  }
  cognito.deleteObject(params).then((data) => {
    if (data) {
      fileDataObj.deleteFile(params).then((data) => {
        res.json({
          status: 200,
          message: 'Delete-channel successfully',
          data: data
        });
      });
    }
  });
});


// Get file data by filename
router.get('/getFileDatabyFilename', (req, res, next) => {

  const params = {
    fileName: req.query.fileName ? req.query.fileName : null
  }
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilename fetched successfully',
      data: data
    });
  });
});


// Get file data by filename
router.get('/getFileDatabyEpgFilename', (req, res, next) => {

  const params = {
    fileName: req.query.fileName ? req.query.fileName : null
  }
  fileDataEpgObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilename fetched successfully',
      data: data
    });
  });
});


// Get file data by filename while [.air file generation]
router.post('/getFileDatabyFilenameForOutput', (req, res, next) => {

  const params = {
    fileName: req.body.fileName ? req.body.fileName : null
  }
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilenameForOutput fetched successfully',
      data: data
    });
  });
});


//srmd DataUpdate
router.post('/wellnessDataUpdate', (req, res, next) => {
  const params = {
    SrNo: req.body.SrNo,
    ProgrammeName: req.body.ProgrammeName,
    TOPIC: req.body.TOPIC,
    Subject: req.body.Subject,
    CodeNo: req.body.CodeNo,
    Duration: req.body.Duration,
    fileName: req.body.fileName,
  }
  fileDataObj.findOneWellnessAndUpdate(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'srmd data Update successfuly',
        data: _response,
      })
    } else {
      console.log('srmd data Update failed');
      res.json({
        status: 401,
        message: 'srmd data Update failed',
        data: null,
      })
    }
  })
})

router.get('/getEPGxdata', (req, res, next) => {

  const params = {
    UserName: req.session.clientName
  }
  fileDataEpgObj.findEPGDataByUserName(params).then((data) => {
    res.json({
      status: 200,
      message: 'Objects fetched successfully',
      data: data,
    });
  });
});

// Delete EPG File from s3 then MongoDb
router.post('/deleteEpg', (req, res, next) => {

  const params = {
    File_name: req.body.File_name ? req.body.File_name : null
  }
  cognito.deleteObjectEpg(params).then((data) => {
    if (data) {
      fileDataEpgObj.deleteFileEpg(params).then((data) => {
        res.json({
          status: 200,
          message: 'Delete-channel successfully',
          data: data
        });
      });
    }
  });
});


module.exports = router;