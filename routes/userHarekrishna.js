var express = require('express');
var router = express.Router();

// ############################################################################ //

var fs = require('fs');
const multerS3 = require('multer-s3');
const awsConfig = require('../config/aws_config')['development'];
var multer = require('multer')
var AWS = require('aws-sdk');
var s3 = new AWS.S3();

const cognito = require('../app/utilities/user_management/cognito');

const fileData = require("../workers/fileData");
const fileDataObj = new fileData.fileDataClass();

const harekrishnaData = require("../workers/harekrishnaData");
const harekrishnaDataObj = new harekrishnaData.harekrishnaDataClass();

const airfileData = require("../workers/airFileData");
const airfileDataObj = new airfileData.airfileDataClass();

// ############################################################################ //

router.get('/playlist_hare_krishna', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userHarekrishna/playlist_hare_krishna', { section: 'Users Listing', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})

router.get('/userHarekrishna', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userHarekrishna/userHarekrishna', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName, clientName: clientName });
})

router.get('/userHarekrishan-xlsheetdata', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('userHarekrishna/playlist_hare_krishna', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})


var channelNameOld = '';
var fileNameInEpg = '';
router.post('/fileHare', (req, res, next) => {
  let fileNAME = '';

  var date = new Date();
  var time = date.toTimeString();
  var withTime = time.substring(0, 8);
  const upload = multer({
    storage: multerS3({
      s3: s3,
      acl: 'public-read',
      bucket: `${awsConfig.bucket}/${awsConfig.putfolder1}`,
      key: (req, file, cb) => {
        console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
        var pieces = `${file.originalname}`.split(".")
        var before = pieces[0]
        var after = pieces[1]
        var adding = before.concat(`_${withTime}`);
        var finalAdd = adding.concat(`.${after}`)
        fileNAME = `${finalAdd}`;
        console.log(`FILE NAME>> ${fileNAME}`);
        cb(null, fileNAME);
      },
    }),
  }).array('userHarekrishna', 1);

  upload(req, res, (error) => {
    if (res) {
      console.log('VIDEO UPLOAD DONE!!!!!!');
      res.send("upload")
      var channel_name = req.body.channel_name ? req.body.channel_name : null
      channelNameOld = channel_name;
      fileNameInEpg = fileNAME;
    } else {
      console.log(`VIDEO UPLOAD ERROR: ${error}`);
    }
  });
})


router.post('/saveplaylist', (req, res, next) => {

  var fileData = req.body.fileData;
  var fileName = fileNameInEpg;
  var UserName = req.session.clientName;
  var channelName = channelNameOld;
  const params = {
    fileData: fileData,
    fileName: fileName,
    UserName: UserName,
    channelName: channelName
  }
  fileDataObj.save(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'Filedata Saved successfuly',
        data: _response,
      })
    } else {
      console.log('Filedata data save failed');
      res.json({
        status: 401,
        message: 'Filedata Save failed',
        data: null,
      })
    }
  })
})

router.post('/getharekrishna', (req, res, next) => {
  const params = {
    ID: req.body.File_Name
  }
  harekrishnaDataObj.findFileDataByUserName(params).then((data) => {
    res.json({
      status: 200,
      message: 'Objects fetched successfully',
      data: data,
    });
  });
});


// Get All Users
router.get('/getXslxdata', (req, res, next) => {
  const params = {
    UserName: req.session.clientName
  }
  fileDataObj.findFileDataByUserName(params).then((data) => {
    res.json({
      status: 200,
      message: 'Objects fetched successfully',
      data: data,
    });
  });
});


// Delete File from s3 then MongoDb
router.post('/deletefile', (req, res, next) => {
  const params = {
    File_name: req.body.File_name ? req.body.File_name : null
  }
  cognito.deleteObject(params).then((data) => {
    if (data) {
      fileDataObj.deleteFile(params).then((data) => {
        res.json({
          status: 200,
          message: 'Delete-channel successfully',
          data: data
        });
      });
    }
  });
});


// // Get file data by filename
router.get('/getFileDatabyFilename', (req, res, next) => {

  const params = {
    fileName: req.query.fileName ? req.query.fileName : null
  }
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilename fetched successfully',
      data: data
    });
  });
});

//srmd DataUpdate
router.post('/hareKDataUpdate', (req, res, next) => {
  const params = {
    SrNo: req.body.SrNo,
    start_time: req.body.start_time,
    end_time: req.body.end_time,
    ProgramName: req.body.ProgramName,
    FileName: req.body.FileName,
    FillersSerialNumber: req.body.FillersSerialNumber,
    POPUPImage_Video_1: req.body.POPUPImage_Video_1,
    POPUPImage_Video_2: req.body.POPUPImage_Video_2,
    POPUPImage_Video_3: req.body.POPUPImage_Video_3,
    fileName: req.body.fileName,
  }
  fileDataObj.findOneAndUpdateHareK(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'srmd data Update successfuly',
        data: _response,
      })
    } else {
      console.log('srmd data Update failed');
      res.json({
        status: 401,
        message: 'srmd data Update failed',
        data: null,
      })
    }
  })
})


module.exports = router;