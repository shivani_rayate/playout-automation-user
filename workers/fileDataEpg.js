const fileDataEpg = require('../app/models/fileDataEpg');


class fileDataEpgCls {
    fileDataEpgCls() { }

    save(params) {
        const fileDataEpgDocument = new fileDataEpg({
            fileName: params.EpgfileName ? params.EpgfileName : null,
            fileData: params.fileDataEpg ? params.fileDataEpg : null,
            UserName: params.UserName ? params.UserName : null,
            ChannelName: params.channelName ? params.channelName : null,
            playlistFilename: params.playlistFilename ? params.playlistFilename : null,
        });
        return new Promise((resolve, reject) => {
            fileDataEpgDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }


    // Find fileData By File Name 
    findUseractivityByUserName(params) {
        return new Promise((resolve, reject) => {
            fileDataEpg.find({ userName: params.userName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    // Find fileData By File Name 
    findbyEpgfileName(params) {
        return new Promise((resolve, reject) => {
            fileDataEpg.find({ fileName: params.playlisyfileName })

            
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    // Find fileData By File Name 
    findFileDataByFileName(params) {
        return new Promise((resolve, reject) => {
            fileDataEpg.findOne({playlistFilename: params.fileName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    findall() {
        return new Promise((resolve, reject) => {
            fileDataEpg.find({})
                .sort({ created_at: -1 })
                .exec((err, channel) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all channel has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(channel);
                    }
                });
        });
    }

    // Delete Channel
    deleteFileEpg(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            fileDataEpg.deleteOne({ fileName: params.File_name })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }

    findEPGDataByUserName(params) {
        return new Promise((resolve, reject) => {
            fileDataEpg.find({ UserName: params.UserName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

}

module.exports = {
    fileDataEpgClass: fileDataEpgCls,
};