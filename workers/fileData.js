const fileData = require('../app/models/fileData');


class fileDataCls {
    fileDataCls() { }

    save(params) {
        const fileDocument = new fileData({
            fileName: params.fileName ? params.fileName : null,
            fileData: params.fileData ? params.fileData : [],
            UserName: params.UserName ? params.UserName : null,
            ChannelName: params.channelName ? params.channelName : null,
        });
        return new Promise((resolve, reject) => {
            fileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }

    // srmd Update in fileData 
    findOneAndUpdate(params) {
        return new Promise((resolve, reject) => {
            fileData.findOne({ fileName: params.fileName }, { fileData: { $elemMatch: { SrNo: params.SrNo } } },
                (err, response) => {
                    if (err) {
                        console.log(`err in findOne Title: ${util.inspect(err)}`);
                        reject(err);
                    } else {
                        if (response.fileData) {

                            fileData.updateOne({
                                "fileName": params.fileName,
                                "fileData.SrNo": params.SrNo
                            }, {

                                $set: {
                                    
                                    "fileData.$.EventID": params.EventID,
                                    "fileData.$.TXTIME": params.TXTIME,
                                    "fileData.$.DATE": params.DATE,
                                    "fileData.$.TITLE": params.TITLE,
                                    "fileData.$.DURATION": params.DURATION
                                }
                            }, { multi: true },
                                (err, fileData) => {
                                    if (err) {
                                        console.log(`err in findOne Title: ${util.inspect(err)}`);
                                        reject(err);
                                    } else {
                                        resolve(fileData);
                                    }
                                });
                        }
                    }
                });
        })
    }


    // Wellness Update in fileData 
    findOneWellnessAndUpdate(params) {
        return new Promise((resolve, reject) => {
            fileData.findOne({ fileName: params.fileName }, { fileData: { $elemMatch: { SrNo: params.SrNo } } },
                (err, response) => {
                    if (err) {
                        console.log(`err in findOne Title: ${util.inspect(err)}`);
                        reject(err);
                    } else {
                        if (response.fileData) {

                            fileData.updateOne({
                                "fileName": params.fileName,
                                "fileData.SrNo": params.SrNo
                            }, {
                                $set: {
                                    "fileData.$.ProgrammeName": params.ProgrammeName,
                                    "fileData.$.TOPIC": params.TOPIC,
                                    "fileData.$.Subject": params.Subject,
                                    "fileData.$.CodeNo": params.CodeNo,
                                    "fileData.$.Duration": params.Duration,
                                }
                            }, { multi: true },
                                (err, fileData) => {
                                    if (err) {
                                        console.log(`err in findOne Title: ${util.inspect(err)}`);
                                        reject(err);
                                    } else {
                                        resolve(fileData);
                                    }
                                });
                        }
                    }
                });
        })
    }

    // Onetake Update in fileData 
    findOneAndUpdateOnetake(params) {
        return new Promise((resolve, reject) => {
            fileData.findOne({ fileName: params.fileName }, { fileData: { $elemMatch: { SrNo: params.SrNo } } },
                (err, response) => {
                    if (err) {
                        console.log(`err in findOne Title: ${util.inspect(err)}`);
                        reject(err);
                    } else {
                        if (response.fileData) {

                            fileData.updateOne({
                                "fileName": params.fileName,
                                "fileData.SrNo": params.SrNo
                            }, {

                                $set: {

                                    "fileData.$.StartDate": params.StartDate,
                                    "fileData.$.ContentID": params.ContentID,
                                    "fileData.$.ProgramName": params.ProgramName,
                                    "fileData.$.ProgramFile": params.ProgramFile,
                                    "fileData.$.OnAirTime": params.OnAirTime,
                                    "fileData.$.DURATION": params.DURATION,

                                }
                            }, { multi: true },
                                (err, fileData) => {
                                    if (err) {
                                        console.log(`err in findOne Title: ${util.inspect(err)}`);
                                        reject(err);
                                    } else {
                                        resolve(fileData);
                                    }
                                });
                        }
                    }
                });
        })
    }

        // Hare K Update in fileData 
        findOneAndUpdateHareK(params) {
            return new Promise((resolve, reject) => {
                fileData.findOne({ fileName: params.fileName }, { fileData: { $elemMatch: { SrNo: params.SrNo } } },
                    (err, response) => {
                        if (err) {
                            console.log(`err in findOne Title: ${util.inspect(err)}`);
                            reject(err);
                        } else {
                            if (response.fileData) {
    
                                fileData.updateOne({
                                    "fileName": params.fileName,
                                    "fileData.SrNo": params.SrNo
                                }, {
                                    $set: {
                                        "fileData.$.AiringStartTime": params.start_time,
                                        "fileData.$.EndTime": params.end_time,
                                        "fileData.$.ProgramName": params.ProgramName,
                                        "fileData.$.FileName": params.FileName,
                                        "fileData.$.FillersSerialNumber": params.FillersSerialNumber,
                                        "fileData.$.POPUPImage/Video_1": params.POPUPImage_Video_1,
                                        "fileData.$.POPUPImage/Video_2": params.POPUPImage_Video_2,
                                        "fileData.$.POPUPImage/Video_3": params.POPUPImage_Video_3,
                                    }
                                }, { multi: true },
                                    (err, fileData) => {
                                        if (err) {
                                            console.log(`err in findOne Title: ${util.inspect(err)}`);
                                            reject(err);
                                        } else {
                                            resolve(fileData);
                                        }
                                    });
                            }
                        }
                    });
            })
        }
    // Find fileData By File Name 
    findUseractivityByUserName(params) {
        return new Promise((resolve, reject) => {
            fileData.find({ userName: params.userName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    // Find fileData By File Name 
    findFileDataByFileName(params) {
        return new Promise((resolve, reject) => {
            fileData.findOne({ fileName: params.fileName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }


    findall() {
        return new Promise((resolve, reject) => {
            fileData.find({})
                .sort({ created_at: -1 })
                .exec((err, channel) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all channel has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(channel);
                    }
                });
        });
    }

    // Delete Channel
    deleteFile(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            fileData.deleteOne({ fileName: params.File_name })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }

    findFileDataByUserName(params) {
        return new Promise((resolve, reject) => {
            fileData.find({ UserName: params.UserName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }
}

module.exports = {
    fileDataClass: fileDataCls,
};