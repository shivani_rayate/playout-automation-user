const airfileData = require('../app/models/airFileData');


class airfileDataCls {
    airfileDataCls() { }


    
    save(params) {
        const fileDocument = new airfileData({
            playlistfileName: params.playlistfileName ? params.playlistfileName : null,
            airfileName: params.airfileName ? params.airfileName : null,
            airfileData : params.airfileData ? params.airfileData : null,
            channelName : params.channelName ? params.channelName : null,
            UserName : params.UserName ? params.UserName : null
        });
        return new Promise((resolve, reject) => {
            fileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: "data saved!",
                        data : data
                    };
                    resolve(response);
                }
            });
        });
    }




    



    // Find fileData By File Name 
    findEPGbyFileName(params) {
        return new Promise((resolve, reject) => {
            epgfileData.findOne({ playlistFilename: params.playlistFilename })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findEPGbyFileName :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {

                        resolve(data);
                       
                    }
                });
        })
    }

}


module.exports = {
    airfileDataClass: airfileDataCls,
};