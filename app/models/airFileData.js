const mongoose = require('mongoose');

const airFileDataSchema = mongoose.Schema({
    playlistfileName: { type: String},
    airfileName: { type: String},
    airfileData: { type: Array, required: true },
    channelName: {type: String},
    UserName: {type: String},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

airFileDataSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('airFileData', airFileDataSchema, 'airFileData');